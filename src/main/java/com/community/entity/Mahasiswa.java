package com.community.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 *
 * @author Java Community
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@ToString
public class Mahasiswa {
    private long id;
    private String nim;
    private String nama;
    private String ttl;
    private String jurusan;
    private String alamat;
}
