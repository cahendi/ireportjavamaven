package com.community.sql;

import com.community.entity.Mahasiswa;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Java Community
 */
public class MahasiswaSQLImpl implements MahasiswaSQL {

    @Override
    public List<Mahasiswa> getDataMahasiswa() throws SQLException{
        Connection con = Koneksi.getKoneksi();
        String query = "SELECT * FROM m_mahasiswa";
        Statement st = null;
        ResultSet rs = null;
        List<Mahasiswa> list = new ArrayList<>();
        try {
            st = con.createStatement();
            rs = st.executeQuery(query);
            while (rs.next()) {
                list.add(Mahasiswa.builder()
                        .id(rs.getLong("id_mahasiswa"))
                        .nim(rs.getString("nim"))
                        .nama(rs.getString("nama"))
                        .ttl(rs.getString("ttl"))
                        .jurusan(rs.getString("jurusan"))
                        .alamat(rs.getString("alamat"))
                        .build());
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            if (con != null) {
                con.close();
            }
            if (st != null) {
                st.close();
            }
            if (rs != null) {
                rs.close();
            }
        }
        return list;
    }
}
