package com.community.controller;

import com.community.entity.Mahasiswa;
import com.community.sql.MahasiswaSQL;
import com.community.sql.MahasiswaSQLImpl;
import com.community.view.ViewPrinting;
import java.io.File;
import java.net.URL;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfExporterConfiguration;
import net.sf.jasperreports.export.SimpleXlsxReportConfiguration;

/**
 *
 * @author Java Community
 */
public class ControllerPrinting {

    private final ViewPrinting view;
    private List<Mahasiswa> list = new ArrayList<>();

    public ControllerPrinting(ViewPrinting view) {
        this.view = view;
        getDataMahasiswa();
    }

    private void getDataMahasiswa() {
        try {
            MahasiswaSQL sql = new MahasiswaSQLImpl();
            list = sql.getDataMahasiswa();

            for (Mahasiswa obj : list) {
                view.getModel().addRow(new Object[]{
                    obj.getNim(), obj.getNama(), obj.getTtl(), obj.getAlamat(), obj.getJurusan()
                });
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Gagal Mengambil Data Mahasiswa");
        }
    }

    private JasperPrint getJasperPrint() throws JRException {
        String path = "/laporan.jrxml";
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource(list);
        JasperReport report = JasperCompileManager.compileReport(getClass().getResourceAsStream(path));
        return JasperFillManager.fillReport(report, null, dataSource);
    }

    public void unduhRepotPDF() {
        try {
            String outputFile = "D://Data-Mahasiswa";
            
            JRPdfExporter exporter = new JRPdfExporter();
            exporter.setExporterInput(new SimpleExporterInput(getJasperPrint()));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputFile + ".pdf"));
            
            SimplePdfExporterConfiguration configuration = new SimplePdfExporterConfiguration();
            exporter.setConfiguration(configuration);

            exporter.exportReport();
        } catch (JRException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Gagal Mengunduh Laporan Data Mahasiswa");
        }
    }

    public void unduhReportXLSX() {
        try {
            String outputFile = "D://Data-Mahasiswa";
            
            JRXlsxExporter exporter = new JRXlsxExporter();
            exporter.setExporterInput(new SimpleExporterInput(getJasperPrint()));
            exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputFile + ".xlsx"));
            
            SimpleXlsxReportConfiguration configuration = new SimpleXlsxReportConfiguration();
            configuration.setOnePagePerSheet(true);
            configuration.setDetectCellType(true);
            configuration.setCollapseRowSpan(false);
            exporter.setConfiguration(configuration);

            exporter.exportReport();
        } catch (JRException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Gagal Mengunduh Laporan Data Mahasiswa");
        }
    }
}
