package com.community;

import com.community.view.ViewPrinting;

/**
 *
 * @author Java Community
 */
public class AppMain {
    public static void main(String[] args) {
        ViewPrinting obj = new ViewPrinting();
        obj.setVisible(true);
    }
}
